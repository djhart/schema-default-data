-- MySQL dump 10.13  Distrib 5.7.5-m15, for osx10.8 (x86_64)
--
-- Host: localhost    Database: edge
-- ------------------------------------------------------
-- Server version	5.7.5-m15-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Dumping data for table `ipsla_category`
--

LOCK TABLES `ipsla_category` WRITE;
/*!40000 ALTER TABLE `ipsla_category` DISABLE KEYS */;
INSERT INTO `ipsla_category` (`ipsla_category_id`, `name`, `description`) VALUES (99000000,'DLSw+','Data Link Switching Plus');
INSERT INTO `ipsla_category` (`ipsla_category_id`, `name`, `description`) VALUES (99000001,'DNS','Domain Name System');
INSERT INTO `ipsla_category` (`ipsla_category_id`, `name`, `description`) VALUES (99000002,'DHCP','Dynamic Host Control Protocol');
INSERT INTO `ipsla_category` (`ipsla_category_id`, `name`, `description`) VALUES (99000003,'FTP','File Transfer Protocol');
INSERT INTO `ipsla_category` (`ipsla_category_id`, `name`, `description`) VALUES (99000004,'HTTP','Hypertext Transfer Protocol');
INSERT INTO `ipsla_category` (`ipsla_category_id`, `name`, `description`) VALUES (99000005,'ICMP Echo','ICMP Echo');
INSERT INTO `ipsla_category` (`ipsla_category_id`, `name`, `description`) VALUES (99000006,'ICMP jitter','ICMP jitter');
INSERT INTO `ipsla_category` (`ipsla_category_id`, `name`, `description`) VALUES (99000007,'ICMP Path Echo','ICMP Path Echo');
INSERT INTO `ipsla_category` (`ipsla_category_id`, `name`, `description`) VALUES (99000008,'ICMP Path Jitter','ICMP Path Jitter');
INSERT INTO `ipsla_category` (`ipsla_category_id`, `name`, `description`) VALUES (99000009,'RTP','Real-Time Transport Protocol');
INSERT INTO `ipsla_category` (`ipsla_category_id`, `name`, `description`) VALUES (99000010,'TCP Connect','Transmission Control');
INSERT INTO `ipsla_category` (`ipsla_category_id`, `name`, `description`) VALUES (99000011,'UDP Echo','UDP Echo');
INSERT INTO `ipsla_category` (`ipsla_category_id`, `name`, `description`) VALUES (99000012,'UDP jitter','UDP jitter');
INSERT INTO `ipsla_category` (`ipsla_category_id`, `name`, `description`) VALUES (99000013,'VoIP','VoIP');
INSERT INTO `ipsla_category` (`ipsla_category_id`, `name`, `description`) VALUES (99000014,'VIDEO','Video');
/*!40000 ALTER TABLE `ipsla_category` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed
