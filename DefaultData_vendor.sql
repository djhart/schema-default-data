-- MySQL dump 10.13  Distrib 5.7.5-m15, for osx10.8 (x86_64)
--
-- Host: localhost    Database: edge
-- ------------------------------------------------------
-- Server version	5.7.5-m15-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Dumping data for table `vendor`
--

LOCK TABLES `vendor` WRITE;
/*!40000 ALTER TABLE `vendor` DISABLE KEYS */;
INSERT INTO `vendor` (`vendor_id`, `name`) VALUES (95000000,'Aprisma');
INSERT INTO `vendor` (`vendor_id`, `name`) VALUES (95000001,'Arbor Networks');
INSERT INTO `vendor` (`vendor_id`, `name`) VALUES (95000002,'BMC');
INSERT INTO `vendor` (`vendor_id`, `name`) VALUES (95000003,'CA');
INSERT INTO `vendor` (`vendor_id`, `name`) VALUES (95000004,'Cisco');
INSERT INTO `vendor` (`vendor_id`, `name`) VALUES (95000005,'Concord');
INSERT INTO `vendor` (`vendor_id`, `name`) VALUES (95000007,'Edge Technologies');
INSERT INTO `vendor` (`vendor_id`, `name`) VALUES (95000006,'EMC');
INSERT INTO `vendor` (`vendor_id`, `name`) VALUES (95000008,'Entuity');
INSERT INTO `vendor` (`vendor_id`, `name`) VALUES (95000009,'Gomez');
INSERT INTO `vendor` (`vendor_id`, `name`) VALUES (95000010,'HP');
INSERT INTO `vendor` (`vendor_id`, `name`) VALUES (95000011,'IBM');
INSERT INTO `vendor` (`vendor_id`, `name`) VALUES (95000012,'Juniper');
INSERT INTO `vendor` (`vendor_id`, `name`) VALUES (95000013,'Lucent');
INSERT INTO `vendor` (`vendor_id`, `name`) VALUES (95000014,'Motorola');
INSERT INTO `vendor` (`vendor_id`, `name`) VALUES (95000015,'Nimsoft');
INSERT INTO `vendor` (`vendor_id`, `name`) VALUES (95000016,'Oracle');
INSERT INTO `vendor` (`vendor_id`, `name`) VALUES (95000017,'Riverbed');
INSERT INTO `vendor` (`vendor_id`, `name`) VALUES (95000018,'SAP');
INSERT INTO `vendor` (`vendor_id`, `name`) VALUES (95000019,'SevOne');
INSERT INTO `vendor` (`vendor_id`, `name`) VALUES (95000020,'Splunk');
INSERT INTO `vendor` (`vendor_id`, `name`) VALUES (95000021,'Tableau');
INSERT INTO `vendor` (`vendor_id`, `name`) VALUES (95000022,'VMware');
INSERT INTO `vendor` (`vendor_id`, `name`) VALUES (95000023,'Xerox');
/*!40000 ALTER TABLE `vendor` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed
