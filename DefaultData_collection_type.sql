-- MySQL dump 10.13  Distrib 5.7.5-m15, for osx10.8 (x86_64)
--
-- Host: localhost    Database: edge
-- ------------------------------------------------------
-- Server version	5.7.5-m15-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Dumping data for table `collection_type`
--

LOCK TABLES `collection_type` WRITE;
/*!40000 ALTER TABLE `collection_type` DISABLE KEYS */;
INSERT INTO `collection_type` (`collection_type_id`, `name`, `description`) VALUES (81000000,'user','User');
INSERT INTO `collection_type` (`collection_type_id`, `name`, `description`) VALUES (81000001,'device','Device Groups');
INSERT INTO `collection_type` (`collection_type_id`, `name`, `description`) VALUES (81000002,'location','Location groups');
INSERT INTO `collection_type` (`collection_type_id`, `name`, `description`) VALUES (81000003,'customer','Customer groups');
INSERT INTO `collection_type` (`collection_type_id`, `name`, `description`) VALUES (81000004,'vendor','Vendor Groups');
INSERT INTO `collection_type` (`collection_type_id`, `name`, `description`) VALUES (81000005,'service','Service Groups');
INSERT INTO `collection_type` (`collection_type_id`, `name`, `description`) VALUES (81000006,'os','Operating System Groups');
INSERT INTO `collection_type` (`collection_type_id`, `name`, `description`) VALUES (81000007,'object','Object Groups');
/*!40000 ALTER TABLE `collection_type` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed
