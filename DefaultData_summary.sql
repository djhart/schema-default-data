-- MySQL dump 10.13  Distrib 5.7.5-m15, for osx10.8 (x86_64)
--
-- Host: localhost    Database: edge
-- ------------------------------------------------------
-- Server version	5.7.5-m15-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Dumping data for table `summary`
--

LOCK TABLES `summary` WRITE;
/*!40000 ALTER TABLE `summary` DISABLE KEYS */;
INSERT INTO `summary` (`summary_id`, `table_name`, `next_available_id`, `total_entries`, `number_default`, `number_custom`) VALUES (1,'location',100013,NULL,NULL,NULL);
INSERT INTO `summary` (`summary_id`, `table_name`, `next_available_id`, `total_entries`, `number_default`, `number_custom`) VALUES (2,'city',631,NULL,NULL,NULL);
INSERT INTO `summary` (`summary_id`, `table_name`, `next_available_id`, `total_entries`, `number_default`, `number_custom`) VALUES (3,'country',NULL,NULL,NULL,NULL);
INSERT INTO `summary` (`summary_id`, `table_name`, `next_available_id`, `total_entries`, `number_default`, `number_custom`) VALUES (4,'link',451,NULL,NULL,NULL);
INSERT INTO `summary` (`summary_id`, `table_name`, `next_available_id`, `total_entries`, `number_default`, `number_custom`) VALUES (5,'application',NULL,NULL,NULL,NULL);
INSERT INTO `summary` (`summary_id`, `table_name`, `next_available_id`, `total_entries`, `number_default`, `number_custom`) VALUES (6,'alert',953,NULL,NULL,NULL);
INSERT INTO `summary` (`summary_id`, `table_name`, `next_available_id`, `total_entries`, `number_default`, `number_custom`) VALUES (7,'domain',NULL,NULL,NULL,NULL);
INSERT INTO `summary` (`summary_id`, `table_name`, `next_available_id`, `total_entries`, `number_default`, `number_custom`) VALUES (8,'datacenter',7,NULL,NULL,NULL);
INSERT INTO `summary` (`summary_id`, `table_name`, `next_available_id`, `total_entries`, `number_default`, `number_custom`) VALUES (9,'device',0,NULL,NULL,NULL);
/*!40000 ALTER TABLE `summary` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed
