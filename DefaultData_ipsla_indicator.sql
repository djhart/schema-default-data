-- MySQL dump 10.13  Distrib 5.7.5-m15, for osx10.8 (x86_64)
--
-- Host: localhost    Database: edge
-- ------------------------------------------------------
-- Server version	5.7.5-m15-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Dumping data for table `ipsla_indicator`
--

LOCK TABLES `ipsla_indicator` WRITE;
/*!40000 ALTER TABLE `ipsla_indicator` DISABLE KEYS */;
INSERT INTO `ipsla_indicator` (`indicator_id`, `name`, `display_name`, `ipsla_category_id`) VALUES (80000000,'dns_availability','',99000001);
INSERT INTO `ipsla_indicator` (`indicator_id`, `name`, `display_name`, `ipsla_category_id`) VALUES (80000001,'dns_avgtime','',99000001);
INSERT INTO `ipsla_indicator` (`indicator_id`, `name`, `display_name`, `ipsla_category_id`) VALUES (80000002,'echo_availability','',99000005);
INSERT INTO `ipsla_indicator` (`indicator_id`, `name`, `display_name`, `ipsla_category_id`) VALUES (80000003,'echo_avgtime','',99000005);
INSERT INTO `ipsla_indicator` (`indicator_id`, `name`, `display_name`, `ipsla_category_id`) VALUES (80000004,'http_availability','',99000004);
INSERT INTO `ipsla_indicator` (`indicator_id`, `name`, `display_name`, `ipsla_category_id`) VALUES (80000005,'http_avgtime','',99000004);
INSERT INTO `ipsla_indicator` (`indicator_id`, `name`, `display_name`, `ipsla_category_id`) VALUES (80000006,'http_dnsRtt','',99000004);
INSERT INTO `ipsla_indicator` (`indicator_id`, `name`, `display_name`, `ipsla_category_id`) VALUES (80000007,'http_httpCode','',99000004);
INSERT INTO `ipsla_indicator` (`indicator_id`, `name`, `display_name`, `ipsla_category_id`) VALUES (80000008,'http_messageBodyBytes','',99000004);
INSERT INTO `ipsla_indicator` (`indicator_id`, `name`, `display_name`, `ipsla_category_id`) VALUES (80000009,'http_sense','',99000004);
INSERT INTO `ipsla_indicator` (`indicator_id`, `name`, `display_name`, `ipsla_category_id`) VALUES (80000010,'http_tcpRtt','',99000004);
INSERT INTO `ipsla_indicator` (`indicator_id`, `name`, `display_name`, `ipsla_category_id`) VALUES (80000011,'http_transactionRtt','',99000004);
INSERT INTO `ipsla_indicator` (`indicator_id`, `name`, `display_name`, `ipsla_category_id`) VALUES (80000012,'icmpjitter_availability','',99000012);
INSERT INTO `ipsla_indicator` (`indicator_id`, `name`, `display_name`, `ipsla_category_id`) VALUES (80000013,'icmpjitter_averageDelayDS','',99000012);
INSERT INTO `ipsla_indicator` (`indicator_id`, `name`, `display_name`, `ipsla_category_id`) VALUES (80000014,'icmpjitter_averageDelaySD','',99000012);
INSERT INTO `ipsla_indicator` (`indicator_id`, `name`, `display_name`, `ipsla_category_id`) VALUES (80000015,'icmpjitter_averageJitter','',99000012);
INSERT INTO `ipsla_indicator` (`indicator_id`, `name`, `display_name`, `ipsla_category_id`) VALUES (80000016,'icmpjitter_averageJitterDS','',99000012);
INSERT INTO `ipsla_indicator` (`indicator_id`, `name`, `display_name`, `ipsla_category_id`) VALUES (80000017,'icmpjitter_averageJitterSD','',99000012);
INSERT INTO `ipsla_indicator` (`indicator_id`, `name`, `display_name`, `ipsla_category_id`) VALUES (80000018,'icmpjitter_averageRtt','',99000012);
INSERT INTO `ipsla_indicator` (`indicator_id`, `name`, `display_name`, `ipsla_category_id`) VALUES (80000019,'icmpjitter_iajin','',99000012);
INSERT INTO `ipsla_indicator` (`indicator_id`, `name`, `display_name`, `ipsla_category_id`) VALUES (80000020,'icmpjitter_iajout','',99000012);
INSERT INTO `ipsla_indicator` (`indicator_id`, `name`, `display_name`, `ipsla_category_id`) VALUES (80000021,'icmpjitter_latePackets','',99000012);
INSERT INTO `ipsla_indicator` (`indicator_id`, `name`, `display_name`, `ipsla_category_id`) VALUES (80000022,'icmpjitter_lostPackets','',99000012);
INSERT INTO `ipsla_indicator` (`indicator_id`, `name`, `display_name`, `ipsla_category_id`) VALUES (80000023,'icmpjitter_negativeJitterAverage','',99000012);
INSERT INTO `ipsla_indicator` (`indicator_id`, `name`, `display_name`, `ipsla_category_id`) VALUES (80000024,'icmpjitter_negativeJitterPercent','',99000012);
INSERT INTO `ipsla_indicator` (`indicator_id`, `name`, `display_name`, `ipsla_category_id`) VALUES (80000025,'icmpjitter_packetLossRatio','',99000012);
INSERT INTO `ipsla_indicator` (`indicator_id`, `name`, `display_name`, `ipsla_category_id`) VALUES (80000026,'icmpjitter_packetsOOS','',99000012);
INSERT INTO `ipsla_indicator` (`indicator_id`, `name`, `display_name`, `ipsla_category_id`) VALUES (80000027,'icmpjitter_packetsOOSDS','',99000012);
INSERT INTO `ipsla_indicator` (`indicator_id`, `name`, `display_name`, `ipsla_category_id`) VALUES (80000028,'icmpjitter_packetsOOSSD','',99000012);
INSERT INTO `ipsla_indicator` (`indicator_id`, `name`, `display_name`, `ipsla_category_id`) VALUES (80000029,'icmpjitter_positiveJitterAverage','',99000012);
INSERT INTO `ipsla_indicator` (`indicator_id`, `name`, `display_name`, `ipsla_category_id`) VALUES (80000030,'icmpjitter_positiveJitterPercent','',99000012);
INSERT INTO `ipsla_indicator` (`indicator_id`, `name`, `display_name`, `ipsla_category_id`) VALUES (80000031,'icmpjitter_sentPackets','',99000012);
INSERT INTO `ipsla_indicator` (`indicator_id`, `name`, `display_name`, `ipsla_category_id`) VALUES (80000032,'jitter_availability','',99000012);
INSERT INTO `ipsla_indicator` (`indicator_id`, `name`, `display_name`, `ipsla_category_id`) VALUES (80000033,'jitter_averageDelayDS','',99000012);
INSERT INTO `ipsla_indicator` (`indicator_id`, `name`, `display_name`, `ipsla_category_id`) VALUES (80000034,'jitter_averageDelaySD','',99000012);
INSERT INTO `ipsla_indicator` (`indicator_id`, `name`, `display_name`, `ipsla_category_id`) VALUES (80000035,'jitter_averageJitter','',99000012);
INSERT INTO `ipsla_indicator` (`indicator_id`, `name`, `display_name`, `ipsla_category_id`) VALUES (80000036,'jitter_averageJitterDS','',99000012);
INSERT INTO `ipsla_indicator` (`indicator_id`, `name`, `display_name`, `ipsla_category_id`) VALUES (80000037,'jitter_averageJitterSD','',99000012);
INSERT INTO `ipsla_indicator` (`indicator_id`, `name`, `display_name`, `ipsla_category_id`) VALUES (80000038,'jitter_averageRtt','',99000012);
INSERT INTO `ipsla_indicator` (`indicator_id`, `name`, `display_name`, `ipsla_category_id`) VALUES (80000039,'jitter_bandwidth','',99000012);
INSERT INTO `ipsla_indicator` (`indicator_id`, `name`, `display_name`, `ipsla_category_id`) VALUES (80000040,'jitter_iajin','',99000012);
INSERT INTO `ipsla_indicator` (`indicator_id`, `name`, `display_name`, `ipsla_category_id`) VALUES (80000041,'jitter_iajout','',99000012);
INSERT INTO `ipsla_indicator` (`indicator_id`, `name`, `display_name`, `ipsla_category_id`) VALUES (80000042,'jitter_icpif','',99000012);
INSERT INTO `ipsla_indicator` (`indicator_id`, `name`, `display_name`, `ipsla_category_id`) VALUES (80000043,'jitter_latePackets','',99000012);
INSERT INTO `ipsla_indicator` (`indicator_id`, `name`, `display_name`, `ipsla_category_id`) VALUES (80000044,'jitter_lostPackets','',99000012);
INSERT INTO `ipsla_indicator` (`indicator_id`, `name`, `display_name`, `ipsla_category_id`) VALUES (80000045,'jitter_mos','',99000012);
INSERT INTO `ipsla_indicator` (`indicator_id`, `name`, `display_name`, `ipsla_category_id`) VALUES (80000046,'jitter_negativeJitterAverage','',99000012);
INSERT INTO `ipsla_indicator` (`indicator_id`, `name`, `display_name`, `ipsla_category_id`) VALUES (80000047,'jitter_negativeJitterPercent','',99000012);
INSERT INTO `ipsla_indicator` (`indicator_id`, `name`, `display_name`, `ipsla_category_id`) VALUES (80000048,'jitter_ntpState','',99000012);
INSERT INTO `ipsla_indicator` (`indicator_id`, `name`, `display_name`, `ipsla_category_id`) VALUES (80000049,'jitter_packetLossDS','',99000012);
INSERT INTO `ipsla_indicator` (`indicator_id`, `name`, `display_name`, `ipsla_category_id`) VALUES (80000050,'jitter_packetLossRatio','',99000012);
INSERT INTO `ipsla_indicator` (`indicator_id`, `name`, `display_name`, `ipsla_category_id`) VALUES (80000051,'jitter_packetLossSD','',99000012);
INSERT INTO `ipsla_indicator` (`indicator_id`, `name`, `display_name`, `ipsla_category_id`) VALUES (80000052,'jitter_packetsOOS','',99000012);
INSERT INTO `ipsla_indicator` (`indicator_id`, `name`, `display_name`, `ipsla_category_id`) VALUES (80000053,'jitter_positiveJitterAverage','',99000012);
INSERT INTO `ipsla_indicator` (`indicator_id`, `name`, `display_name`, `ipsla_category_id`) VALUES (80000054,'jitter_positiveJitterPercent','',99000012);
INSERT INTO `ipsla_indicator` (`indicator_id`, `name`, `display_name`, `ipsla_category_id`) VALUES (80000055,'jitter_sentPackets','',99000012);
INSERT INTO `ipsla_indicator` (`indicator_id`, `name`, `display_name`, `ipsla_category_id`) VALUES (80000056,'jitter_sigmaDelayDS','',99000012);
INSERT INTO `ipsla_indicator` (`indicator_id`, `name`, `display_name`, `ipsla_category_id`) VALUES (80000057,'jitter_sigmaDelaySD','',99000012);
INSERT INTO `ipsla_indicator` (`indicator_id`, `name`, `display_name`, `ipsla_category_id`) VALUES (80000058,'jitter_sigmaJitterDS','',99000012);
INSERT INTO `ipsla_indicator` (`indicator_id`, `name`, `display_name`, `ipsla_category_id`) VALUES (80000059,'jitter_sigmaJitterSD','',99000012);
INSERT INTO `ipsla_indicator` (`indicator_id`, `name`, `display_name`, `ipsla_category_id`) VALUES (80000060,'jitter_sigmaRtt','',99000012);
INSERT INTO `ipsla_indicator` (`indicator_id`, `name`, `display_name`, `ipsla_category_id`) VALUES (80000061,'jitter_unSyncRtts','',99000012);
INSERT INTO `ipsla_indicator` (`indicator_id`, `name`, `display_name`, `ipsla_category_id`) VALUES (80000062,'rtp_MOSCQDS','',99000009);
INSERT INTO `ipsla_indicator` (`indicator_id`, `name`, `display_name`, `ipsla_category_id`) VALUES (80000063,'rtp_MOSCQSD','',99000009);
INSERT INTO `ipsla_indicator` (`indicator_id`, `name`, `display_name`, `ipsla_category_id`) VALUES (80000064,'rtp_MOSLQSD','',99000009);
INSERT INTO `ipsla_indicator` (`indicator_id`, `name`, `display_name`, `ipsla_category_id`) VALUES (80000065,'rtp_RFactorDS','',99000009);
INSERT INTO `ipsla_indicator` (`indicator_id`, `name`, `display_name`, `ipsla_category_id`) VALUES (80000066,'rtp_RFactorSD','',99000009);
INSERT INTO `ipsla_indicator` (`indicator_id`, `name`, `display_name`, `ipsla_category_id`) VALUES (80000067,'rtp_availability','',99000009);
INSERT INTO `ipsla_indicator` (`indicator_id`, `name`, `display_name`, `ipsla_category_id`) VALUES (80000068,'rtp_averageDelayDS','',99000009);
INSERT INTO `ipsla_indicator` (`indicator_id`, `name`, `display_name`, `ipsla_category_id`) VALUES (80000069,'rtp_averageDelaySD','',99000009);
INSERT INTO `ipsla_indicator` (`indicator_id`, `name`, `display_name`, `ipsla_category_id`) VALUES (80000070,'rtp_frameLostDS','',99000009);
INSERT INTO `ipsla_indicator` (`indicator_id`, `name`, `display_name`, `ipsla_category_id`) VALUES (80000071,'rtp_interarrivalJitterDS','',99000009);
INSERT INTO `ipsla_indicator` (`indicator_id`, `name`, `display_name`, `ipsla_category_id`) VALUES (80000072,'rtp_interarrivalJitterSD','',99000009);
INSERT INTO `ipsla_indicator` (`indicator_id`, `name`, `display_name`, `ipsla_category_id`) VALUES (80000073,'rtp_packetLoss','',99000009);
INSERT INTO `ipsla_indicator` (`indicator_id`, `name`, `display_name`, `ipsla_category_id`) VALUES (80000074,'rtp_packetLossDS','',99000009);
INSERT INTO `ipsla_indicator` (`indicator_id`, `name`, `display_name`, `ipsla_category_id`) VALUES (80000075,'rtp_packetLossSD','',99000009);
INSERT INTO `ipsla_indicator` (`indicator_id`, `name`, `display_name`, `ipsla_category_id`) VALUES (80000076,'rtp_packetsEarlyDS','',99000009);
INSERT INTO `ipsla_indicator` (`indicator_id`, `name`, `display_name`, `ipsla_category_id`) VALUES (80000077,'rtp_packetsLateDS','',99000009);
INSERT INTO `ipsla_indicator` (`indicator_id`, `name`, `display_name`, `ipsla_category_id`) VALUES (80000078,'rtp_packetsLostDS','',99000009);
INSERT INTO `ipsla_indicator` (`indicator_id`, `name`, `display_name`, `ipsla_category_id`) VALUES (80000079,'rtp_packetsLostSD','',99000009);
INSERT INTO `ipsla_indicator` (`indicator_id`, `name`, `display_name`, `ipsla_category_id`) VALUES (80000080,'rtp_packetsMIA','',99000009);
INSERT INTO `ipsla_indicator` (`indicator_id`, `name`, `display_name`, `ipsla_category_id`) VALUES (80000081,'rtp_packetsOOSSD','',99000009);
INSERT INTO `ipsla_indicator` (`indicator_id`, `name`, `display_name`, `ipsla_category_id`) VALUES (80000082,'rtp_packetsSentDS','',99000009);
INSERT INTO `ipsla_indicator` (`indicator_id`, `name`, `display_name`, `ipsla_category_id`) VALUES (80000083,'rtp_packetsSentSD','',99000009);
INSERT INTO `ipsla_indicator` (`indicator_id`, `name`, `display_name`, `ipsla_category_id`) VALUES (80000084,'rtp_responseTime','',99000009);
INSERT INTO `ipsla_indicator` (`indicator_id`, `name`, `display_name`, `ipsla_category_id`) VALUES (80000085,'tcpconnect_availability','',99000010);
INSERT INTO `ipsla_indicator` (`indicator_id`, `name`, `display_name`, `ipsla_category_id`) VALUES (80000086,'tcpconnect_avgtime','',99000010);
INSERT INTO `ipsla_indicator` (`indicator_id`, `name`, `display_name`, `ipsla_category_id`) VALUES (80000087,'udpecho_availability','',99000011);
INSERT INTO `ipsla_indicator` (`indicator_id`, `name`, `display_name`, `ipsla_category_id`) VALUES (80000088,'udpecho_avgtime','',99000011);
INSERT INTO `ipsla_indicator` (`indicator_id`, `name`, `display_name`, `ipsla_category_id`) VALUES (80000089,'video_availability','',99000014);
INSERT INTO `ipsla_indicator` (`indicator_id`, `name`, `display_name`, `ipsla_category_id`) VALUES (80000090,'video_averageDelaySD','',99000014);
INSERT INTO `ipsla_indicator` (`indicator_id`, `name`, `display_name`, `ipsla_category_id`) VALUES (80000091,'video_averageJitterSD','',99000014);
INSERT INTO `ipsla_indicator` (`indicator_id`, `name`, `display_name`, `ipsla_category_id`) VALUES (80000092,'video_iajout','',99000014);
INSERT INTO `ipsla_indicator` (`indicator_id`, `name`, `display_name`, `ipsla_category_id`) VALUES (80000093,'video_ipdvAverageJitter','',99000014);
INSERT INTO `ipsla_indicator` (`indicator_id`, `name`, `display_name`, `ipsla_category_id`) VALUES (80000094,'video_latePackets','',99000014);
INSERT INTO `ipsla_indicator` (`indicator_id`, `name`, `display_name`, `ipsla_category_id`) VALUES (80000095,'video_lostPackets','',99000014);
INSERT INTO `ipsla_indicator` (`indicator_id`, `name`, `display_name`, `ipsla_category_id`) VALUES (80000096,'video_negativeJitterAverage','',99000014);
INSERT INTO `ipsla_indicator` (`indicator_id`, `name`, `display_name`, `ipsla_category_id`) VALUES (80000097,'video_negativeJitterPercent','',99000014);
INSERT INTO `ipsla_indicator` (`indicator_id`, `name`, `display_name`, `ipsla_category_id`) VALUES (80000098,'video_ntpState','',99000014);
INSERT INTO `ipsla_indicator` (`indicator_id`, `name`, `display_name`, `ipsla_category_id`) VALUES (80000099,'video_packetLossRatio','',99000014);
INSERT INTO `ipsla_indicator` (`indicator_id`, `name`, `display_name`, `ipsla_category_id`) VALUES (80000100,'video_packetLossSD','',99000014);
INSERT INTO `ipsla_indicator` (`indicator_id`, `name`, `display_name`, `ipsla_category_id`) VALUES (80000101,'video_packetsOOS','',99000014);
INSERT INTO `ipsla_indicator` (`indicator_id`, `name`, `display_name`, `ipsla_category_id`) VALUES (80000102,'video_positiveJitterAverage','',99000014);
INSERT INTO `ipsla_indicator` (`indicator_id`, `name`, `display_name`, `ipsla_category_id`) VALUES (80000103,'video_positiveJitterPercent','',99000014);
INSERT INTO `ipsla_indicator` (`indicator_id`, `name`, `display_name`, `ipsla_category_id`) VALUES (80000104,'video_sentPackets','',99000014);
INSERT INTO `ipsla_indicator` (`indicator_id`, `name`, `display_name`, `ipsla_category_id`) VALUES (80000105,'video_unSyncRtts','',99000014);
INSERT INTO `ipsla_indicator` (`indicator_id`, `name`, `display_name`, `ipsla_category_id`) VALUES (80000106,'voip_availability','',99000013);
INSERT INTO `ipsla_indicator` (`indicator_id`, `name`, `display_name`, `ipsla_category_id`) VALUES (80000107,'voip_avgtime','',99000013);
/*!40000 ALTER TABLE `ipsla_indicator` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed
