-- MySQL dump 10.13  Distrib 5.7.5-m15, for osx10.8 (x86_64)
--
-- Host: localhost    Database: edge
-- ------------------------------------------------------
-- Server version	5.7.5-m15-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Dumping data for table `edge_default_mapping`
--

LOCK TABLES `edge_default_mapping` WRITE;
/*!40000 ALTER TABLE `edge_default_mapping` DISABLE KEYS */;
INSERT INTO `edge_default_mapping` (`edge_default_mapping_id`, `table`, `starting_value`, `starting_value_readable`, `purpose`) VALUES (1,'country',91000000,'91 Million','countries');
INSERT INTO `edge_default_mapping` (`edge_default_mapping_id`, `table`, `starting_value`, `starting_value_readable`, `purpose`) VALUES (2,'governing_district',92000000,'92 Million','State, Province, County entries');
INSERT INTO `edge_default_mapping` (`edge_default_mapping_id`, `table`, `starting_value`, `starting_value_readable`, `purpose`) VALUES (3,'city',93000000,'93 Million','Cities');
INSERT INTO `edge_default_mapping` (`edge_default_mapping_id`, `table`, `starting_value`, `starting_value_readable`, `purpose`) VALUES (4,'governing_district_type',94000000,'94 Million','Type: State, Province, County');
INSERT INTO `edge_default_mapping` (`edge_default_mapping_id`, `table`, `starting_value`, `starting_value_readable`, `purpose`) VALUES (5,'vendor',95000000,'95 Million','Cisco, IBM, SevOne');
INSERT INTO `edge_default_mapping` (`edge_default_mapping_id`, `table`, `starting_value`, `starting_value_readable`, `purpose`) VALUES (6,'application',96000000,'96 Million','Omnibus,Appboard,SevOnePAS,Outlook');
INSERT INTO `edge_default_mapping` (`edge_default_mapping_id`, `table`, `starting_value`, `starting_value_readable`, `purpose`) VALUES (7,'location',90000000,'90 Million','Default Locations');
INSERT INTO `edge_default_mapping` (`edge_default_mapping_id`, `table`, `starting_value`, `starting_value_readable`, `purpose`) VALUES (8,'location_type',97000000,'97 Million','city,country,building,datacenter');
INSERT INTO `edge_default_mapping` (`edge_default_mapping_id`, `table`, `starting_value`, `starting_value_readable`, `purpose`) VALUES (9,'domain',98000000,'98 Million','System, Public, CustomerA');
INSERT INTO `edge_default_mapping` (`edge_default_mapping_id`, `table`, `starting_value`, `starting_value_readable`, `purpose`) VALUES (10,'ipsla_category',99000000,'99 Million','HTTP,TCP Connect, Echo');
INSERT INTO `edge_default_mapping` (`edge_default_mapping_id`, `table`, `starting_value`, `starting_value_readable`, `purpose`) VALUES (11,'ipsla_indicator',80000000,'80 Million','echo_availability,http_avgtime');
INSERT INTO `edge_default_mapping` (`edge_default_mapping_id`, `table`, `starting_value`, `starting_value_readable`, `purpose`) VALUES (12,'collection_type',81000000,'81 Million','users, user groups,device groups, locations, customers');
INSERT INTO `edge_default_mapping` (`edge_default_mapping_id`, `table`, `starting_value`, `starting_value_readable`, `purpose`) VALUES (13,'timezone',82000000,'82 Million','timezones');
INSERT INTO `edge_default_mapping` (`edge_default_mapping_id`, `table`, `starting_value`, `starting_value_readable`, `purpose`) VALUES (14,'capital_type',83000000,'83 Million','Governing District, Country, State');
INSERT INTO `edge_default_mapping` (`edge_default_mapping_id`, `table`, `starting_value`, `starting_value_readable`, `purpose`) VALUES (15,'protocol',84000000,'84 Million','TCP, RDP, ICMP, IGMP...etc');
INSERT INTO `edge_default_mapping` (`edge_default_mapping_id`, `table`, `starting_value`, `starting_value_readable`, `purpose`) VALUES (16,'protocol_type',85000000,'85 Million','IP,ATM,IPX');
INSERT INTO `edge_default_mapping` (`edge_default_mapping_id`, `table`, `starting_value`, `starting_value_readable`, `purpose`) VALUES (17,'device_type',86000000,'86 Million','chassis,interface,module');
INSERT INTO `edge_default_mapping` (`edge_default_mapping_id`, `table`, `starting_value`, `starting_value_readable`, `purpose`) VALUES (18,'cos',87000000,'87 Million','CS0,CS2,CS6 IPSLA Class of Service Types');
INSERT INTO `edge_default_mapping` (`edge_default_mapping_id`, `table`, `starting_value`, `starting_value_readable`, `purpose`) VALUES (19,'scope',88000000,'88 Million','global, vendor, application');
INSERT INTO `edge_default_mapping` (`edge_default_mapping_id`, `table`, `starting_value`, `starting_value_readable`, `purpose`) VALUES (20,'status_type',89000000,'89 Million','IPSLA, alert.  Combined with scope.');
INSERT INTO `edge_default_mapping` (`edge_default_mapping_id`, `table`, `starting_value`, `starting_value_readable`, `purpose`) VALUES (21,'api_type',70000000,'70 Million','Type of API: SOAP,Java,Perl..');
/*!40000 ALTER TABLE `edge_default_mapping` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed
